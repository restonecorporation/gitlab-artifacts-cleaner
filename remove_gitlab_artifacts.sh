#!/bin/bash
#
# Written by Vincent Nijhuis
# GitHub: https://bitbucket.org/account/user/restonecorporation/projects/GT
# Email: vincent.nijhuis@rest-one.nl
# Website: https://rest-one.nl
#
# Note: This code is a stop-gap to erase Job Artifacts for a project. I HIGHLY recommend you leverage
#       "artifacts:expire_in" in your .gitlab-ci.yml
#
# https://docs.gitlab.com/ee/ci/yaml/#artifactsexpire_in
#
# Software Requirements: curl, jq
#
# This code has been released under the terms of the Apache-2.0 license
# http://opensource.org/licenses/Apache-2.0


# per_page var to determine how many jobs per pages
per_page="50"
# starting_page to set which page to start cleaning, 1 all jobs. 2 keep last 50 jobs
starting_page="2"

# project_id, find it here: https://gitlab.com/[organization name]/[repository name] at the top underneath repository name
project_ids=([1]=reponame [2]=reponame [3]=reponame [4]=reponame)
# project_id="1" #reponame

# token, find it here: https://gitlab.com/profile/personal_access_tokens
token="GITLABUSERTOKEN"
server="GITLABURL"

for projectid in ${!project_ids[@]}; do
  echo ""
  echo "The project ID ${projectid} for ${project_ids[${projectid}]}"

  # Creating list of Job IDs for the Project specified with Artifacts
  job_ids=()

  # Retrieving Jobs list page count
  total_pages=$(curl -sD - -o /dev/null -X GET \
    "https://$server/api/v4/projects/$projectid/jobs?per_page=$per_page" \
    -H "PRIVATE-TOKEN: ${token}" | grep -Fi X-Total-Pages | sed 's/[^0-9]*//g')

  function get_artefacts_to_clean () {
    for ((i=${starting_page};i<=${total_pages};i++)) #starting with page 2 skipping most recent 50 Jobs
    do
      echo "Processing Page: ${i}/${total_pages}"
      response=$(curl -s -X GET \
        "https://$server/api/v4/projects/$projectid/jobs?per_page=$per_page&page=${i}" \
        -H "PRIVATE-TOKEN: ${token}")
      length=$(echo $response | jq '. | length')
      for ((j=0;j<${length};j++))
      do
        if [[ $(echo $response | jq ".[${j}].artifacts_file | length") > 0 ]]; then
            echo "Job found: $(echo $response | jq ".[${j}].id")"
            job_ids+=($(echo $response | jq ".[${j}].id"))
        fi
      done
    done
  }

  function erase_old_artefacts () {
    for job_id in ${job_ids[@]};
    do
      response=$(curl -s -X POST \
        "https://$server/api/v4/projects/$projectid/jobs/$job_id/erase" \
        -H "PRIVATE-TOKEN:${token}")
      echo "Processing Job ID: ${job_id} - Status: $(echo $response | jq '.status')"
    done
  }

  echo ""
  echo "Creating list of all Job ids that currently have Artifacts for ${project_ids[${projectid}]}"
  echo "Total Pages: ${total_pages}"

  # Call get_artefacts_to_clean function line 44
  get_artefacts_to_clean

  # Loop through each Job erasing the Artifact(s)
  echo ""
  echo "${#job_ids[@]} Jobs found. Commencing removal of Artifacts for ${project_ids[${projectid}]}"

  # Call erase_old_artefacts function line 62
  erase_old_artefacts

  echo "=================================================================================================================="
done
